#!/bin/sh
# speed.sh: a very tiny utility to measure typing speed.
# from: https://www.linuxjournal.com/content/how-fast-can-you-type-develop-tiny-utility-bash-find-out, 2011
# 2018 by gm: some minor fixes, translated to German
prompt="Schreibe den obigen Text ('Kannst du...') bis einschließlich 'Starten mit' ab.\nDrücke die Enter-Taste und Strg+D, wenn du fertig bist."
echo -e "\n$prompt \n"
start_time=`date +%s`
words=`cat|wc -w`
end_time=`date +%s`
speed=`echo "scale=2; $words / ( ( $end_time - $start_time ) / 60)" | bc`
echo -e "\n\nDeine Schreibgeschwindigkeit beträgt $speed Wörter pro Minute.\n"
