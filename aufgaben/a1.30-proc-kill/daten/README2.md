Einen Prozess finden
====================

Jetzt geht es darum, den Prozess von Mr. X zu finden.

Im Dateinamen des Programms ist die Zeichenkette "mrx" zu finden.
Finde diese Zeile in der Ausgabe von

    ps -axu

und achte drauf, dass in der ersten Spalte dein Username steht.

Merke dir dann die PID aus der zweiten Spalte.

Weiter geht es mit README3.md
