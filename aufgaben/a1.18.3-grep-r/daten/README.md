grep -r
=======

Zuerst erzeugen wir uns eine schöne Menge an Daten. Führe dazu
folgenden Befehl aus:

    ./create_data2.sh

Untersuche das Verzeichnis chat2 und stelle fest, dass es eine
Menge Dateien enthält. Es ist weiterer Chat, der auf mehrere Dateien
aufgeteilt ist.

Mit
    grep -r TEXT

kann man ab dem aktuellen Verzeichnis in allen Unterverzeichnissen
nach Text suchen.

Aufgabe 2:
    Suche in allen Dateien nach dem Text "Gute Nacht".
    Wieviele Treffer gibt es? --> $ST_CURRENTWORKDIR/ergebnis2

Aufgabe 3:

    Mit der Option -i kann man ohne Rücksicht auf Groß- und Kleinschreibung suchen.
    Das i steht für "case-insensitive".

    grep -ri TEXT

    Suche in allen Dateien nochmal nach "gute nacht", diesmal mit der i-Option.
    Wieviele Treffer gibt es? --> $ST_CURRENTWORKDIR/ergebnis3

Hinweis, die i-Option funktioniert auf ohne das -r.

