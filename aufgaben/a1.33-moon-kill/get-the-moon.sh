#!/bin/bash

# https://www.cyberciti.biz/faq/bash-infinite-loop/
while true
do
    # mit pgrep für den aktuellen User nach dem Prozess moon-buggy suchen
    while [[ -z $(pgrep --uid $(id --user) moon-buggy) ]]
    do
        sleep 1
        # echo wait for moon-buggy...
    done

    # echo moon-buggy startet / Zeit läuft...
    # 20 Sekunden
    sleep 20
    pkill --uid $(id --user) moon-buggy
    echo
    echo "X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X"
    echo
    echo "  Die Lizenz ist abgelaufen. Moon-Buggy wurde gestoppt."
    echo "  Überweise 100 Bitcoins, um die Spieldauer um 2 Minuten zu verlängern."
    echo "  -- Es grüßt dich... Mr. X"
    echo
    echo "X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X X"
    echo
done
