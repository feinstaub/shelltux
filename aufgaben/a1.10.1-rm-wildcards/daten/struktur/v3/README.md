Aufgabe
=======

Der Stern-Platzhalter ist vielseitig einsetzbar:

ls a*     -  Dateien auflisten, die mit a beginnen
ls *b     -  Dateien auflisten, die mit b enden
ls a*z    -  Dateien auflisten, die mit a beginnen und mit z enden
ls *abc*  -  Dateien auflisten, die abc im Namen enthalten

Es gibt auch eckige Klammern:

ls [aA]*  -  Dateien auflisten, die mit a oder A beginnen

AUFGABE:

Dieses Verzeichnis enthält neben den unerwünschten
Mr.-X-Dateien auch solche, die nicht gelöscht werden sollen.
Es sollen alle Dateien gelöscht werden, ...

1. ...die mit "mrx" *beginnen*
2. ...die mit "XX" *enden*
3. ...die *irgendwo* "ABC" (großgeschrieben) enthalten

Die Groß-/Kleinschreibung ist jeweils zu beachten.

TIPP: Mit ls plus Platzhalter kannst du prüfen, ob das richtige gelöscht
wird, bevor du den Platzhalter mit rm verwendest.

Wenn du fertig bist, prüfe deine Lösung mit shelltux solve.

Weitere Informationen zu Platzhaltern:

- https://debian-blog.org/wildcards-die-platzhalter/
- http://tldp.org/LDP/GNU-Linux-Tools-Summary/html/x11655.htm
