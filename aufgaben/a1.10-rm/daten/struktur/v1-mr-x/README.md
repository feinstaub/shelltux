Aufgabe 1
=========

Alle Dateien aus diesem Verzeichnis sollen gelöscht werden
(inklusive der README.md)

Zum Löschen einer Datei gibt es diesen Befehl:

    rm DATEI

rm steht für englisch "remove" (= "entfernen").
                       ^ ^
Prüfe zwischendurch mit ls, ob die Dateien wirklich weg sind.

Wenn du fertig bist, schaue dir die README.md im
Verzeichnis v2-mr-x an.
