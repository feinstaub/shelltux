ShellTux - Installation
=======================

Schritte zur Installation.

**HINWEIS: wenn Fragen zu dieser Anleitung bestehen oder etwas unklar ist, bitte unter [Issues](https://framagit.org/feinstaub/shelltux/issues) ein Ticket aufmachen, um Hilfe zu erhalten.**


### Einmalig auf dem System installieren

Das git-Repository klonen (ggf. muss **git** nachinstalliert werden).
Zum Beispiel direkt ins Home-Verzeichnis.
Oder anderswohin; der Codestand wird auf dem Rechner nur an einer Stelle benötigt.

    cd ~
    git clone https://framagit.org/feinstaub/shelltux.git

(Hinweis: Angenommen ~ = /home/USER, dann wird das Verzeichnis **/home/USER/shelltux** erzeugt. Im Folgenden wird dafür immer die ~ verwendet.)

Die Konsolen-Ausgabe sieht ungefähr so aus:

    Cloning into 'shelltux'...
    remote: Counting objects: 3228, done.
    remote: Compressing objects: 100% (181/181), done.
    remote: Total 3228 (delta 133), reused 257 (delta 116), pack-reused 2911
    Receiving objects: 100% (3228/3228), 4.25 MiB | 1.10 MiB/s, done.
    Resolving deltas: 100% (1705/1705), done.


### Fehlende Pakete nachinstallieren

    cd ~/shelltux
    ./shelltux init --checkenv

Überall dort, wo in der Ausgabe NEIN steht, muss etwas nachinstalliert werden.

Beispielhafte Ausgabe, falls alle benötigten Programme installiert sind:

    Notwendige Programme oder Programme, die früh gebraucht werden:
    figlet                        JA
    bc                            JA
    cowsay                        JA
    dot                           JA
    Aufgabenspezifische Programme:
    [...]

Bei den ersten 4 Programmen muss überall ein JA stehen, damit ShellTux funktioniert.
Gegebenenfalls also nachinstallieren.

Hinweis: das Programm dot ist im Paket `graphviz` enthalten.

Die "Aufgabenspezifischen Programme" sind optional.


### Einmalig pro Benutzer initialisieren

Falls mit mehreren Benutzern auf einem System gearbeitet wird, dann mit jedem Benutzer einloggen und folgendes tun. Ansonsten das Folgende mit dem aktuellen Benutzer durchführen.

    cd ~/shelltux
    source $(pwd)/shelltux init

Damit wurde in die **.bashrc** ein Alias eingetragen, so dass ab der Aufruf `shelltux` von jedem Verzeichnis aus möglich ist.

Die Ausgabe sieht so aus:

    Alias auf shelltux einrichten und ggf. in /home/USER/.bashrc schreiben...
    alias shelltux='. /home/USER/shelltux/shelltux'
    .bashrc angepasst.
    Nächste Schritte:
    1. Mit 'shelltux init --checkenv' prüfen, ob alle notwendigen Pakete installiert sind.
    2. Dann gebe shelltux ein und drücke ENTER.


### ShellTux starten

Der Benutzer kann nun

    shelltux

eingeben, um die Aufgaben zu starten.

    Willkommen bei
    ____  _          _ _ _____
    / ___|| |__   ___| | |_   _|   ___  __
    \___ \| '_ \ / _ \ | | | || | | \ \/ /
    ___) | | | |  __/ | | | || |_| |>  <
    |____/|_| |_|\___|_|_| |_| \__,_/_/\_\

    dem interaktiven Bash-Shell-Tutorial


### Update / Upgrade

Wenn es Updates gibt, können diese mit diesem Befehl installiert werden:

    shelltux update

Der Datei ~/shelltux/CHANGELOG stehen die letzten Änderungen zu entnehmen.


### "Merkblatt gesamt" ansehen

Im Laufe der Übungen erstellen die Teilnehmer Schritt für Schritt ein Merkblatt.
Um auf einen Blick zu sehen, was alles gelernt wird, kann man sich das
gesamte Merkblatt ausgeben lassen.

    shelltux status --merkblatt | less
